#!/usr/bin/env python3

import csv
from random import shuffle, choices, choice

csv_data = ""
script = ""
quizmaster = ""
contestants = ""
questions = ""

class Question(object):
    """docstring for Question."""

    def __init__(self, q, ca, ica):
        super(Question, self).__init__()
        self.question = q
        self.correct_answer = ca
        self.incorrect_answer = ica

    def __str__(self):
        return "["+self.question+" '"+self.correct_answer +"'; '"+self.incorrect_answer+"']"

class Actor(object):
    """docstring for Actor."""

    def __init__(self, name):
        super(Actor, self).__init__()
        self.name = name
        self.score = 0
        self.times_named = 0
        self.position = 0

    def __str__(self):
        return "Actor(\""+self.name+"\")"

    def ask_question(self, actor_asked, question):
        log()
        actor_asked.times_named += 1

    def answer_question(self, answer, is_correct):
        if is_correct:
            score += 1


def log_utterance(speaker, recipient, utterance):
    event_type = "utterance"
    cnt = speaker.name
    rct = recipient.name
    data = utterance

    _log(event_type +","+ cnt +","+ rct +","+ data)

def _log(event):
    """log an event to csv output"""
    global csv_data

    csv_data += event+'\n'

def get_questions(filename):
    questions = []
    with open(filename, newline='\n') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        questions = [Question(row["question"], row["correct_answer"], row["incorrect_answer"]) for row in reader]
    return questions

def my_print(string):
    global script
    print(string)
    script += "\n"+string


def main():
    global quizmaster, contestants

    # First, get the 6 names to be used.
    names_1 = ["John", "Maria", "Elizabeth", "Annie", "Carlos", "Susan"]
    names_2 = ["Michael", "Emma", "Megan", "Paul", "Carol", "Amy"]

    questions = get_questions("trivia.csv")
    shuffle(questions)

    for first_script in [True, False]:

        if first_script:
            names = names_1
        else:
            names = names_2

        shuffle(names)
        contestants = [Actor(name) for name in names[:-1]]
        quizmaster = Actor(names[-1])
        quizmaster.position = 0

        # Now one has been assigned quiz master (position 0),
        # the rest have been shuffled around the table.

        for i in range(len(contestants)):
            contestants[i].position = i+1

        # Print a header with info on characters,
        contestant_positions()
        my_print("------------")

        # Now start the script with the quizmaster intro.
        quizmaster_intro()

        # Go through each question

        question_range = range(9) if first_script else range(10,19)

        for i in question_range:
            question_loop(questions[i])
            my_print("\n"+quizmaster.name+": 'Next question..'\n")
        final_question = questions[9] if first_script else questions[19]
        question_loop(final_question)

        my_print(quizmaster.name+": 'That's the end of the round. Thank you all for playing.'")
        my_print("-----------\nFinal scores:")
        for c in contestants:
            my_print(c.name +" "+ str(c.score) + "      (named "+str(c.times_named)+" times)")

    #for q in get_questions("trivia.csv"):
    #    print(q)
    write_script_to_file("script.txt")

def write_script_to_file(filename):
    global script
    with open(filename, "w") as file:
        file.write(script)

def contestant_positions():
    global quizmaster, contestants
    my_print("Quizmaster: "+quizmaster.name)
    for c in contestants:
        my_print("Contestant "+str(c.position)+": "+c.name)

def quizmaster_intro():
    global quizmaster, contestants

    my_print(quizmaster.name+":")
    my_print("'Welcome, and thank you for taking part in this experiment.'\n"
            + "[2-second pause]\n\n"
            + "'Feel free to get comfortable with the controls, try looking around the room a bit [gesture around the room]. You can ask a researcher if you need help with this.'\n"
            + "[5-second pause. Quiz master can organise papers]\n\n"
            + "'The people seated around you here will partake in a quiz, I will be the quiz master. Follow along while this is happening. I will ask a question to a contestant, and two contestants will answer at the same time. I will then say which one of the two was correct. For each question, you are asked to write down the *correct answer* that was given, of the two answers. The answer might be hard to make out; try to write as much of it as you can, or leave it blank if it was too hard to hear at all.'\n\n"
            + "'Once or twice, the quiz will pause, and you will be asked to answer a few short questions about it before resuming.'\n\n"
            + "'Don't forget - this isn't a test of your personal knowledge, there's no \"right answer\". You can stop the experiment at any time.'\n"
            +"[2-second pause]\n\n")
    my_print("[Look at contestant to the left] 'Let's do a round of introductions.'")

    for c in contestants:
        my_print(c.name+": 'Hi, my name is "+c.name+".'")
        my_print("[2-second pause]")

    my_print(quizmaster.name+": 'I'm "+quizmaster.name+", and I'll be the quiz master this round. [pause]")
    my_print(quizmaster.name+": 'Let's begin.'\n\n")

def question_loop(question):
    global quizmaster, contestants

    current_contestant = choices(contestants)[0]
    #print("Current contestant:",current_contestant.name)
    distractor_position = select_distractor(current_contestant.position)
    for c in contestants:
        if c.position == distractor_position:
            distractor = c
            break

    #print("current distractor:",distractor.name)

    correct = choice([True, False])

    q_statement = quizmaster.name+": '"+question.question+" "+current_contestant.name+"?'"
    current_contestant.times_named += 1

    q_statement += "\n"+current_contestant.name+": 'I think the answer is-'"

    if correct:
        a_statement = current_contestant.name+": '"+question.correct_answer+"'"
        d_statement = distractor.name+": '"+question.incorrect_answer+"'"
        f_statement = quizmaster.name+": 'That's right, "+current_contestant.name+".'"
        current_contestant.times_named += 1
        current_contestant.score += 1
    else:
        a_statement = current_contestant.name+": '"+question.incorrect_answer+"'"
        d_statement = distractor.name+": '"+question.correct_answer+"'"
        f_statement = quizmaster.name+": 'That's right, "+distractor.name+".'"
        distractor.times_named += 1
        distractor.score += 1

    my_print(q_statement)
    my_print(a_statement+"   "+d_statement)
    my_print(f_statement)


def select_distractor(position):

    possible_offsets = [1, 2, 3, 4, 5]
    possible_positions = [(x+position)%6 for x in possible_offsets]

    weights = [0.05, 0.15, 0.6, 0.15, 0.05]

    # remove
    for i in range(5):
        if possible_positions[i] == 0:
            possible_positions.pop(i)
            weights.pop(i)
            break

    return choices(possible_positions, weights = weights)[0]

if __name__ == '__main__':
    main()
