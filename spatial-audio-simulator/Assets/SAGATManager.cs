using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SAGATManager : MonoBehaviour
{
    // TODO: crossfade the canvas with https://answers.unity.com/questions/780323/unity-ui-fading-canvaspanel.html

    //public Button[] answerButtons;
    public Button continueButton;
    public CameraTurner cameraTurner;
    private ToggleGroup toggleGroup;
    [ShowOnly] public bool clip1Selected;
    [ShowOnly] public int sagatTime1;
    [ShowOnly] public int sagatTime2;

    [Range(1,12)]
    public int questionToPick = 1;

    [Header("Clip 1")]
    public float trialTime1;
    public float[] sTimes1;
    public string quizMaster1;
    public string[] contestants1;

    [Header("Clip 2")]
    public float trialTime2;
    public float[] sTimes2;
    public string quizMaster2;
    public string[] contestants2;
    private string alreadyDidDirection = "";
    private List<int> alreadyAsked;
    private bool alreadyDid4 = false;

    public struct SAGATQuestion {
        //Variable declaration
        public string Question;
        public string[] Answers;
        public string CorrectAnswer;

        //Constructor (not necessary, but helpful)
        public SAGATQuestion(string question) {
            this.Question = question;
            this.Answers = new string[6];
            this.CorrectAnswer = "none";
        }
        // Note: does not include the "i dont know" answer
    }

    private List<SAGATQuestion> currentQuestions;
    private SAGATQuestion currentQuestion;

    // Start is called before the first frame update
    void Start()
    {
        continueButton.gameObject.SetActive(false);
        toggleGroup = gameObject.GetComponentInChildren<ToggleGroup>();
        currentQuestions = new List<SAGATQuestion>();
        alreadyAsked = new List<int>();

        //cameraTurner = GameObject.GetComponent<CameraTurner>();
    }

    public void Toggle()
    {
        continueButton.gameObject.SetActive(toggleGroup.AnyTogglesOn());
        foreach (Toggle t in toggleGroup.ActiveToggles())
        {
            Logger.Instance.Log("Toggle pressed: "+t.GetComponentInChildren<Text>().text);
        }

    }

    public void ResetButtons()
    {
        toggleGroup.SetAllTogglesOff();
        continueButton.gameObject.SetActive(false);
    }

    public Vector2 PickSagatTimes(bool clip1) // to be called by the experiment manager
    {
        clip1Selected = clip1;

        if (TestParameters.trialSession){
            return new Vector2((clip1 ? trialTime1 : trialTime2), 0);
        }

        float[] sTimes;

        if (clip1)
        {
            sTimes = sTimes1;
        }
        else
        {
            sTimes = sTimes2;
        }

        int firstSTIndex1, firstSTIndex2, secondSTIndex1, secondSTIndex2;

        firstSTIndex1 = 0;
        firstSTIndex2 = -1;
        secondSTIndex1 = -1;
        secondSTIndex2 = sTimes.Length-1;

        // First, get last time and remove a minute, then pick a time at or before that
        float latestFirstST =  sTimes[sTimes.Length-1] - 60;
        if (latestFirstST < 60) Debug.LogError("Latest first SAGAT time selected is too early ("+latestFirstST+")");

        // Now find the latest index of the list that this is under
        for (int i = sTimes.Length-1; i>=0; i--)
        {
            if (sTimes[i] <= latestFirstST)
            {
                //Debug.Log("Yet!");
                firstSTIndex2 = i;
                //Debug.Log("Got latest first sagat time index " + i + " ("+sTimes[i]+")");
                break;
            }
            else{
              //Debug.Log("Not yet.. on "+sTimes[i]);
            }
        }

        if (firstSTIndex2 == 0) Debug.LogError("Couldn't find proper SAGAT times! No window found for first SAGAT");

        // So we have the lower boundary of the first, and the upper boundaries of both SAGAT times.
        // Next get the first time fully...
        int firstSTIndexFinal = Random.Range(firstSTIndex1, firstSTIndex2+1);
        float firstSagatTime = sTimes[firstSTIndexFinal];
        Debug.Log("Got first SAGAT time: "+firstSagatTime+" (index "+firstSTIndexFinal+")");

        // Now get the second time, starting at one minute after the first.
        float earliestSecondST = firstSagatTime + 60;
        for (int i = firstSTIndexFinal; i<sTimes.Length; i++)
        {
            if (sTimes[i] >= earliestSecondST)
            {
                secondSTIndex1 = i;
                break;
            }
        }

        if (secondSTIndex1 == -1) {
            Debug.LogWarning("Couldn't find proper SAGAT times! No window found for second SAGAT, trying again with some changes");
            // Reduce first sagat and try again
            firstSagatTime = sTimes[firstSTIndexFinal-1];

            // Now get the second time, starting at one minute after the first.
            earliestSecondST = firstSagatTime + 60;
            for (int i = firstSTIndexFinal; i<sTimes.Length; i++)
            {
                if (sTimes[i] > earliestSecondST)
                {
                    secondSTIndex1 = i;
                    break;
                }
            }

            if (secondSTIndex1 == -1) {
                Debug.LogError("Still couldnt find it :( ");
                Application.Quit();
            }

        }


        int secondSTIndexFinal = Random.Range(secondSTIndex1, secondSTIndex2+1);
        float secondSagatTime = sTimes[secondSTIndexFinal];
        Debug.Log("Got second SAGAT time: "+secondSagatTime+" (index "+secondSTIndexFinal+")");

        Logger.Instance.Log("Got SAGAT times: "+firstSagatTime+"s and "+secondSagatTime+"s");

        sagatTime1 = (int)firstSagatTime;
        sagatTime2 = (int)secondSagatTime;

        return new Vector2(sagatTime1, sagatTime2);
    }

    public void ContinueButtonPress()
    {
        Debug.Log("Continue button pressed");
        bool gottem = false;
        // first check if toggle is active, then get its value
        //Debug.Log("Active toggles found: "+toggleGroup.ActiveToggles().Count());

        string answer = "";

        foreach (Toggle t in toggleGroup.ActiveToggles())
        {
          if (gottem){
            Debug.LogError("More than one answer selected for SAGAT questionnaire");
          }
          else
          {
            gottem = true;

            // We have the first answer, get its data
            answer = t.gameObject.GetComponentInChildren<Text>().text;
            Debug.Log("Answer selected: "+answer);
            break;
          }
        }
        if (!gottem)
        {
            Debug.LogError("No toggles found");
        }

        // Log the answer
        bool answerCorrect = (currentQuestion.CorrectAnswer.Contains(answer));
        Logger.Instance.Log("SAGAT answer given: \""+answer+"\" ("+ (answerCorrect ? "correct" : "incorrect") +"; \""+currentQuestion.CorrectAnswer+"\")");

        // either load the next sagat question or fade back to the video
        if (currentQuestions.Count>0){
            // Still questions to go, do the next one
            LoadSagatQuery(currentQuestions[0]);
            currentQuestions.RemoveAt(0);
        }
        else
        {
            // fade back to video
            cameraTurner.lockMovement = false;
            ExperimentManager.Instance.EndSagat();

        }
    }

    public void shuffle(string[] texts)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < texts.Length; t++ )
        {
            string tmp = texts[t];
            int r = Random.Range(t, texts.Length);
            texts[t] = texts[r];
            texts[r] = tmp;
        }
    }

    public SAGATQuestion GenerateSAGATQuestion(int type)
    {
        SAGATQuestion q = new SAGATQuestion();
        q.Question = "";
        q.Answers = new string[]{"","","","","",""};
        q.CorrectAnswer = "none";


        Timekeeper.ClipQuestion[] cqs = Timekeeper.Instance.GetAllPriorQuestions();
        Timekeeper.ClipQuestion lastQuestion = Timekeeper.Instance.GetLastQuestion();
        //string name;
        //int count;

        // Don't repeat 4
        if (alreadyDid4 && type == 4) while (type == 4) type = Random.Range(1,10);



        // ok. this is going to be long
        switch(type){
          case 1: // Case 1:
            q.Question = "Identify one person who was in your field of view just before the simulation paused.";
            // Don't bother making a valid answer for this, we'll do it after

            // Now this will work as is, but let's see if we can find the correct answer without too much difficulty

            q.Answers = AllNames();
            shuffle(q.Answers);

            float viewAngle = cameraTurner.transform.eulerAngles.y;
            int position = 0;

            if (viewAngle > 330 && viewAngle <= 360 || viewAngle >= 0 && viewAngle <= 30){
                position = 5;
            }
            else if (viewAngle > 30 && viewAngle <= 90){
                position = 0;
            }
            else if (viewAngle > 90 && viewAngle <= 150){
                position = 1;
            }
            else if (viewAngle > 150 && viewAngle <= 210){
                position = 2;
            }
            else if (viewAngle > 210 && viewAngle <= 270){
                position = 3;
            }
            else if (viewAngle > 270 && viewAngle <= 330){
                position = 4;
            }
            else
            {
                Debug.LogError("Unrecognised current angle: "+viewAngle);
            }

            q.CorrectAnswer = AllNames()[position];
            // Add adjacent people (so its a chill question)
            q.CorrectAnswer += "|"+AllNames()[(position+1)%6];
            q.CorrectAnswer += "|"+AllNames()[(position-1)%6];

            break;

            case 2:
              //different (l/r) for both sagat queries
              //Random.value > 0.5f
              q.Question = "Who is sitting two seats to the [l/r] of the quiz master (your [l/r])?";

              string direction;
              if (alreadyDidDirection == "left") direction = "right";
              else if (alreadyDidDirection == "right") direction = "left";
              else {
                  direction = Random.value > 0.5f ? "left" : "right";
                  alreadyDidDirection = direction;
              }

              q.Question = q.Question.Replace("[l/r]",direction);

              if (direction == "left") q.CorrectAnswer = clip1Selected ? "Susan" : "Carol";
              else q.CorrectAnswer = clip1Selected ? "Elizabeth" : "Emma";

              q.Answers = AllNames();
              shuffle(q.Answers);


              break;
          case 3: // REQUIREMENT: 6 or more questions
            q.Question = "What was the most recent quiz question?";
            //take pool of all questions asked to date
            q.CorrectAnswer = lastQuestion.question;

            int x = 0;
            while (x<6 && x<cqs.Length-1){
                q.Answers[x] = cqs[cqs.Length-(x+1)].question;
                x++;
            }

            // worst damn code I've ever written
            while (x<6)
            {
              q.Answers[x] = "What continent covers four hemispheres?";
              x++;

              if (x<6){
                  q.Answers[x] = "What pigment is mercurial sulfate?";
                  x++;
              }
              if (x<6){
                  q.Answers[x] = "How long is the longest river in the UK?";
                  x++;
              }
              if (x<6){
                  q.Answers[x] = "What is the tallest building in Europe?";
                  x++;
              }
              if (x<6){
                  q.Answers[x] = "How many players are there in a cricket match?";
                  x++;
              }
            }

            shuffle(q.Answers);
            break;

          case 4:
            q.Question = "What kind of social event is taking place here?";
            q.Answers = new string[]{"Quiz","Debate","Focus Group","Presentation","Exam","Workshop"};
            shuffle(q.Answers);
            q.CorrectAnswer = "Quiz";
            alreadyDid4 = true;
            break;

          case 5:
            q.Question = "What is the colour of the quiz master's shirt?";

            q.Answers = new string[]{"Brown","Green","Blue","Yellow","White","Black"};
            shuffle(q.Answers);

            if (clip1Selected) q.CorrectAnswer = "Brown";
            else q.CorrectAnswer = "Green";

            shuffle(q.Answers);

            break;

          case 6:
            //different (l/r) for both sagat queries
            //Random.value > 0.5f
            q.Question = "Who is sitting [l/r] of the quiz master (your [l/r])?";

            string direction2;
            if (alreadyDidDirection == "left") direction2 = "right";
            else if (alreadyDidDirection == "right") direction2 = "left";
            else {
                direction2 = Random.value > 0.5f ? "left" : "right";
                alreadyDidDirection = direction2;
            }

            q.Question = q.Question.Replace("[l/r]",direction2);

            if (direction2 == "left") q.CorrectAnswer = clip1Selected ? "Maria" : "Paul";
            else q.CorrectAnswer = clip1Selected ? "John" : "Michael";

            q.Answers = AllNames();
            shuffle(q.Answers);

            break;

          case 7:
            q.Question = "Who was asked to answer a question most recently?";
            q.CorrectAnswer = lastQuestion.target;
            q.Answers = AllNames();
            shuffle(q.Answers);
            break;

          case 8:
            q.Question = "Who is the quiz master?";
            q.CorrectAnswer = clip1Selected ? quizMaster1 : quizMaster2;
            q.Answers = AllNames();
            shuffle(q.Answers);
            break;

          case 9:
            q.Question = "Who last answered incorrectly?";
            q.CorrectAnswer = lastQuestion.targetCorrect ? lastQuestion.distractor : lastQuestion.target;
            q.Answers = AllNames();
            shuffle(q.Answers);
            break;

          case 10:
            q.Question = "Who last answered correctly?";
            q.CorrectAnswer = lastQuestion.targetCorrect ? lastQuestion.target : lastQuestion.distractor;
            q.Answers = AllNames();
            shuffle(q.Answers);
            break;

          // The following two are kinda too difficult.

          /**
          case 11:
            q.Question = "Who last answered at the same time as [name]?";
            // Pick name based on existing names
            // NOTE: Consider removing this one, it's a bit hard to think about

            int qIndex = Random.Range(0,cqs.Length);
            name = (Random.value > 0.5f) ? cqs[qIndex].distractor : cqs[qIndex].target;
            // Nice
            q.Question = q.Question.Replace( "[name]", name );
            // Got the name, now look back and find when they last participated

            for (int i = cqs.Length-1; i >= 0; i--)
            {
                if (cqs[i].target == name){
                  q.CorrectAnswer = cqs[i].distractor;
                  break;
                }
                else if (cqs[i].distractor == name)
                {
                  q.CorrectAnswer = cqs[i].target;
                  break;
                }
            }

            q.Answers = AllNames();
            shuffle(q.Answers);

            for (int i = 0; i < 6; i++){
              if (q.Answers[i] == name){
                q.Answers[i] = name+" has not answered any questions";
              }
            }

            break;

          case 12:
            q.Question = "Who do you think is going to have the most right answers at the end?";
            q.Answers = AllNames();
            int highestScore = 0;
            foreach (string a in q.Answers){
                if (Timekeeper.Instance.scores[a] > highestScore){
                    q.CorrectAnswer = a;
                    highestScore = Timekeeper.Instance.scores[a];
                }
                else if (Timekeeper.Instance.scores[a] == highestScore){
                    q.CorrectAnswer += "|"+a;
                }
            }
            break;**/

        }
        return q;
    }

    public void StartSagat(int whichSagat)
    {
        cameraTurner.lockMovement = true;
        // generate SAGAT questions
        List<int> numbersUsed = new List<int>();

        while (numbersUsed.Count < 5){
            int num = Random.Range(1,11);
            if (!numbersUsed.Contains(num) && !(num == 8 && alreadyAsked.Contains(8))){
                numbersUsed.Add(num);
            }
        }

        // Remember which ones we asked
        alreadyAsked.AddRange(numbersUsed);

        // Generate current questions
        foreach (int n in numbersUsed){
            currentQuestions.Add(GenerateSAGATQuestion(n));
        }

        // Load question 1
        LoadSagatQuery(currentQuestions[0]);
        currentQuestions.RemoveAt(0);
    }

    public string RandomContestant(bool clip1){
        int nameIndex = Random.Range(0,5);
        string name = clip1 ? contestants1[nameIndex] : contestants2[nameIndex];
        return name;
    }

    public string RandomContestant(){
        return RandomContestant(clip1Selected);
    }

    public string[] AllNames(){
        string[] allNames = new string[6];
        for (int i = 0; i < 5; i++)
        {
            allNames[i] = clip1Selected ? contestants1[i] : contestants2[i];
        }
        allNames[5] = clip1Selected ? quizMaster1 : quizMaster2;
        return allNames;
    }

    public void LoadSagatQuery(SAGATQuestion q)
    {
        ResetButtons();
        currentQuestion = q;
        int i = 0;
        foreach (Toggle child in toggleGroup.GetComponentsInChildren<Toggle>()){
            //Debug.Log("Got toggle; "+child.gameObject.name);
            child.GetComponentInChildren<Text>().text = q.Answers[i];
            i++;
            if (i >= 6) break;
        }

        transform.Find("SAGAT").Find("Question").GetComponentInChildren<Text>().text = q.Question;

        string aStr = "";
        foreach (string s in q.Answers){
            if (s == q.CorrectAnswer) aStr += "["+s+"] ; ";
            else aStr += s+" ; ";
        }

        transform.GetComponentInChildren<AnswerButtonIconManager>().UpdateIcons();

        Logger.Instance.Log("SAGAT question loaded: \""+q.Question+"\" Answers: "+aStr+")");
    }

    public void testSagatQuestion(int number){
        Debug.Log("Generating SAGAT Question...");
        SAGATQuestion q = GenerateSAGATQuestion(number);
        Debug.Log("SAGAT Question "+number+": \""+q.Question+"\"");
        string aStr = "";
        foreach (string s in q.Answers){
            if (s == q.CorrectAnswer) aStr += "["+s+"] ; ";
            else aStr += s+" ; ";
        }
        Debug.Log("Answers: "+aStr);
    }


    [ContextMenu("Test chosen SAGAT Question")]
    public void testSagatQuestionChosen(){
        testSagatQuestion(questionToPick);
    }

    [ContextMenu("Test Random SAGAT Question")]
    public void testSagatQuestionRandom(){
        testSagatQuestion(Random.Range(1,11));
    }


    [ContextMenu("Test all SAGAT questions")]
    public void testAllSagatGen(){
        for (int i = 1; i<11; i++){
          SAGATQuestion q = GenerateSAGATQuestion(i);
          Debug.Log("SAGAT Question "+i+": \""+q.Question+"\"");
          string aStr = "";
          foreach (string s in q.Answers){
              if (s == q.CorrectAnswer) aStr += "["+s+"] ; ";
              else aStr += s+" ; ";
          }
          Debug.Log("Answers: "+aStr);
          Debug.Log("Correct answer: "+q.CorrectAnswer);
        }

    }

}
