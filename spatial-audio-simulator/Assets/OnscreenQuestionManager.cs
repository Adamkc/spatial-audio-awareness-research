using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnscreenQuestionManager : MonoBehaviour
{

    // This script manages question logging in its entirety.

    public Text answer1;
    public Text answer2;

    public int currentIndex;
    public int timeToAnswer = 5;

    private Timekeeper tk;
    private CanvasGroup cg;

    [HideInInspector]
    public bool questionShowing = false;
    private Timekeeper.ClipQuestion currentQuestion;
    // Start is called before the first frame update
    void Start()
    {
       tk = Timekeeper.Instance;
       currentIndex = tk.GetIndex();
       cg = GetComponent<CanvasGroup>();
       cg.alpha = 0f;

       Logger.Instance.Log("RightAnswerTask: Ready");
    }

    // Update is called once per frame
    void Update()
    {
        if (tk.GetIndex() > currentIndex)
        {
            // Update and show new question
            currentIndex = tk.GetIndex();
            currentQuestion = tk.GetLastQuestion();

            ShowQuestion(currentQuestion);
            Logger.Instance.Log("RightAnswerTask: Showing onscreen question "+currentIndex);

        }
        else if (questionShowing)
        {
            // Log responses or hide if too late

            if (Input.GetKey("a"))
            {
                // Answer logged: 1
                string answer = answer1.text;

                bool correct;
                if (currentQuestion.targetCorrect) correct = (answer == currentQuestion.targetAnswer);
                else correct = (answer == currentQuestion.distractorAnswer);

                string logstring = "RightAnswerTask: " + (correct ? "Correct" : "Incorrect") +" answer "+currentIndex+" logged";
                logstring += " (" + (currentQuestion.targetCorrect ? "Target" : "Distractor") + " correct, A pressed)";

                Logger.Instance.Log(logstring);
                HideQuestion();


            }
            else if (Input.GetKey("d"))
            {
                // Answer logged: 2
                string answer = answer2.text;

                bool correct;
                if (currentQuestion.targetCorrect) correct = (answer == currentQuestion.targetAnswer);
                else correct = (answer == currentQuestion.distractorAnswer);

                string logstring = "RightAnswerTask: " + (correct ? "Correct" : "Incorrect") +" answer "+currentIndex+" logged";
                logstring += " (" + (currentQuestion.targetCorrect ? "Target" : "Distractor") + " correct, D pressed)";

                Logger.Instance.Log(logstring);
                HideQuestion();



            }
            else if (tk.GetTime() > currentQuestion.time + timeToAnswer)
            {
                Logger.Instance.Log("RightAnswerTask: Timeout, no answer given");
                HideQuestion();
            }

        }
    }

    public void ShowQuestion(Timekeeper.ClipQuestion cq)
    {
        // Show the question passed with cq, shuffle left and right etc

        // shuffle
        bool targetFirst = (Random.value > 0.5f);

        answer1.text = targetFirst ? cq.targetAnswer : cq.distractorAnswer;
        answer2.text = targetFirst ? cq.distractorAnswer : cq.targetAnswer;

        cg.alpha = 1f;
        questionShowing = true;
    }

    public void HideQuestion()
    {
        // Hide question. Could be already hidden.
        cg.alpha = 0f;
        questionShowing = false;
    }
}
