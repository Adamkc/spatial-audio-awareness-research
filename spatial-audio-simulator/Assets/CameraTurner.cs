using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTurner : MonoBehaviour
{

    public GameObject cameraTarget;
    public float accelleration;
    public float topSpeed = 50;

    [ShowOnly] public bool lockMovement;

    [Space(10)]
    public bool lerpSmoothing = true;
    public float lerpRate = 1;
    public float pitchChangeSensitivity = 1; //1 degree



    private float previousPitch;
    private Transform from;
    private Transform to;


    // Start is called before the first frame update
    void Start()
    {
        previousPitch = transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {

        if (!lockMovement)
        {

            float targetSpeed = 0;

            // Handle turning with keys
            if (Input.GetKey("left"))
            {
                targetSpeed = -topSpeed;
            }
            else if (Input.GetKey("right"))
            {
                targetSpeed = topSpeed;
            }
            else targetSpeed = 0;


            if (lerpSmoothing)
            {
                cameraTarget.transform.Rotate(new Vector3(0, targetSpeed, 0) * Time.deltaTime);

                // Now get quaternion and lerp
                from = transform;
                to = cameraTarget.transform;

                transform.rotation = Quaternion.Lerp(from.rotation, to.rotation, Time.time * lerpRate);
            }
            else
            {
                transform.Rotate(new Vector3(0, targetSpeed, 0) * Time.deltaTime);
            }


            // Log pitch

            float pitch = transform.eulerAngles.y;
            // Log current pitch if the change was above a certain angle
            float currentPitchDifference = Mathf.DeltaAngle(pitch, previousPitch);

            if (Mathf.Abs(currentPitchDifference) > pitchChangeSensitivity){
                // Log angle

                Logger.Instance.Log("Camera angle: "+pitch);

                previousPitch = pitch;
            }
        }

    }

}
