using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


public class ExperimentManager : MonoBehaviour
{

    protected static ExperimentManager instance;

    public static ExperimentManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(ExperimentManager)) as ExperimentManager;

            return instance;
        }
    }

    public Camera userCamera;
    public AudioSource audioSource;
    public AudioMixerGroup resonanceMixerGroup;
    public AudioMixerGroup monoMixerGroup;
    public VideoPlayer videoPlayer;
    public Fader canvasFader;

    [Space(10)]
    public int participantId = -1;

    [Header("Test conditions")]
    public bool clip1Selected = true;
    public bool forceMono = false;
    public bool trialSession = false;

    [Space(10)]
    public int debugTimeSkip = 0;

    [Header("SAGAT Times")]
    [ShowOnly] public int sTime1;
    [ShowOnly] public bool sagat1Done = false;
    [ShowOnly] public int sTime2;
    [ShowOnly] public bool sagat2Done = false;

    [Header("Clip 1 Trial")]
    public VideoClip videoClip1Trial;
    public AudioClip audioClip1Trial;
    public AudioClip audioClip1MonoTrial;

    [Header("Clip 1")]
    public VideoClip videoClip1;
    public AudioClip audioClip1;
    public AudioClip audioClip1Mono;

    public float audioStartDelay1;

    [Header("Clip 2 Trial")]
    public VideoClip videoClip2Trial;
    public AudioClip audioClip2Trial;
    public AudioClip audioClip2MonoTrial;

    [Header("Clip 2")]
    public VideoClip videoClip2;
    public AudioClip audioClip2;
    public AudioClip audioClip2Mono;

    public float audioStartDelay2;

    protected SAGATManager sagatManager;

    protected bool mediaReady = false;

    protected float audioStartDelay = 0;
    protected bool waitingForDelay = false;
    protected bool audioFirst = false;

    protected bool sagatUnderway = false;
    protected bool videoOver = false;

    protected bool usingClip;

    [HideInInspector]
    public string videoExtension = "webm";

    void Awake()
    {
        TestParameters.trialSession = trialSession;

        // Find if Windows or Linux
        if (Application.isEditor)
        {
          Debug.Log("Editor detected, going webm");
          videoExtension = "webm";
        }
        else videoExtension = "mp4";

        #if (UNITY_STANDALONE_WIN && !UNITY_EDITOR)
            Debug.Log("Windows detected, going mp4");
            videoExtension = "mp4";
        #endif

        #if UNITY_STANDALONE_LINUX
            Debug.Log("Standalone Linux detected, going webm");
            videoExtension = "webm";
        #endif


        if (videoClip1 == null || videoClip2 == null) usingClip = false;
        else usingClip = true;

        if (usingClip) Debug.Log("Unpacking internal video"); else Debug.Log("Using external .webm");



        // first lets get params
        string id = TestParameters.participantId;

        if (id == null)
        {
          Debug.LogWarning("No participant ID detected");
          id = "-1";
        }

        participantId = int.Parse(id);
        // Ok so we need to see how to lay out participant ID info here...

        // 4 test conditions:
        // 0: Clip 1 Mono, Clip 2 Spatial
        // 1: Clip 1 Spatial, Clip 2 Mono
        // 2: Clip 2 Mono, Clip 1 Spatial
        // 3: Clip 2 Spatial, Clip 1 Mono

        // That's obsolete now since we're doing between-subjects testing. Here's the new one:

        // 0: Mono. Clip 1 trial, clip 2 test
        // 1: Spatial. Clip 1 trial, clip 2 test
        // 2: Mono. Clip 2 trial, clip 1 test
        // 3: Spatial. Clip 2 trial, clip 1 test

        // Participant ID should determine these, so basically the condition is modulo 4 of the id

        int userScenario = participantId % 4;

        bool clip1First, mono;

        switch (userScenario){
            case 0:
                // 0: Mono. Clip 1 trial, clip 2 test
                clip1First = true;
                mono = true;
                break;
            case 1:
                // 1: Spatial. Clip 1 trial, clip 2 test
                clip1First = true;
                mono = false;
                break;
            case 2:
                // 2: Mono. Clip 2 trial, clip 1 test
                clip1First = false;
                mono = true;
                break;
            case 3:
                // 3: Spatial. Clip 2 trial, clip 1 test
                clip1First = false;
                mono = false;
                break;
            default:
                // just to shut up the compiler

                Logger.Instance.Log("Warning: No test parameters detected, randomly generating instead");
                //Debug.LogWarning("what the logger said");
                clip1First = (Random.value > 0.5f);
                mono = (Random.value > 0.5f);
                break;
        }

        // ok now we have all the params we need, lets find which vlip to use and which audio condition
        if (trialSession){
            clip1Selected = clip1First;
        } else { // second viewing
            clip1Selected = !clip1First;
        }
        forceMono = mono;

        // ... its done!!?? really?


    }
    // Start is called before the first frame update
    void Start()
    {

        TestParameters.trialSession = trialSession;


        Logger.Instance.Log("Participant ID: "+TestParameters.participantId);
        Logger.Instance.Log("Session: "+(trialSession ? "Trial" : "Actual"));
        Logger.Instance.Log("Scene: "+(clip1Selected ? "Scene 1" : "Scene 2"));
        Logger.Instance.Log("Audio: "+(forceMono ? "Mono" : "Spatial"));

        // Here, find which audio/video is required, line them up in the players

        Timekeeper.Instance.clip1Selected = clip1Selected;
        Timekeeper.Instance.UpdateClip();

        if (clip1Selected)
        {
            if (forceMono){
                audioSource.outputAudioMixerGroup = monoMixerGroup;
                SetAudioClip(trialSession ? audioClip1MonoTrial : audioClip1Mono);
            }
            else
            {
                audioSource.outputAudioMixerGroup = resonanceMixerGroup;
                SetAudioClip(trialSession ? audioClip1Trial : audioClip1);

            }

            if (usingClip) videoPlayer.clip = videoClip1;

            else // from File
            {

                string file = Application.dataPath;
                string[] pathArray = file.Split('/');
                file="";
                for(int i=0 ; i<pathArray.Length-1 ; i++){
                    file += pathArray[i]+"/";
                }

                string filename = trialSession ? "Videos/Scene1_trial." : "Videos/Scene1.";

                Logger.Instance.Log("Loading video from file: " + file + filename + videoExtension);

                videoPlayer.url = file + filename + videoExtension;

            }
            audioStartDelay = audioStartDelay1;
        }
        else
        {
            if (forceMono){
                audioSource.outputAudioMixerGroup = monoMixerGroup;
                SetAudioClip(trialSession ? audioClip2MonoTrial : audioClip2Mono);
            }
            else
            {
                audioSource.outputAudioMixerGroup = resonanceMixerGroup;
                SetAudioClip(trialSession ? audioClip2Trial : audioClip2);
            }

            if (usingClip) videoPlayer.clip = videoClip2;

            else // from File
            {
                string file = Application.dataPath;
                string[] pathArray = file.Split('/');
                file="";
                for(int i=0 ; i<pathArray.Length-1 ; i++){
                    file += pathArray[i]+"/";
                }

                string filename = trialSession ? "Videos/Scene2_trial." : "Videos/Scene2.";


                Logger.Instance.Log("Loading video from file: " + file + filename + videoExtension);

                videoPlayer.url = file + filename + videoExtension;

            }
            audioStartDelay = audioStartDelay2;
        }

        audioSource.Stop();
        videoPlayer.Stop();

        canvasFader.FadeOut();


        // Wait till both video and audio are loaded before continuing
        videoPlayer.Prepare();


        // pick sagat times
        sagatManager = canvasFader.gameObject.GetComponentInChildren<SAGATManager>();
        Vector2 sTimes = sagatManager.PickSagatTimes(clip1Selected);
        sTime1 = (int)sTimes.x;
        sTime2 = (int)sTimes.y;
        Debug.Log("Got SAGAT Times: "+sTime1+", "+sTime2);
        Debug.Log("Loading media...");


    }

    public void SetAudioClip(AudioClip clip)
    {
        Debug.Log("Audio clip: "+clip.name);
        audioSource.clip = clip;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape"))  WrapItUp("Escape pressed during "+(trialSession ? "trial" : "experiment"));

        // Preparation part 1: make sure media is ready
        if (!mediaReady){
          mediaReady = (videoPlayer.isPrepared && audioSource.clip.loadState == AudioDataLoadState.Loaded);
          if (mediaReady)
          {
            Debug.Log("Media loaded. Starting experiment...");
            // Handle differences in audio/video
            if (audioStartDelay != 0)
            {
                audioSource.Stop();
                videoPlayer.Stop();
                waitingForDelay = true;

                if (audioStartDelay > 0) // audio starts after video
                {
                    audioFirst = false;
                    audioSource.PlayDelayed(audioStartDelay);
                    videoPlayer.Play();
                    waitingForDelay = false;
                }
                else // video starts after audio
                {
                    audioFirst = true;
                    audioSource.Play();
                }
            }
            else // both start at once, no delay
            {
                audioSource.Play();
                videoPlayer.Play();
            }

            #if UNITY_EDITOR
            if (debugTimeSkip > 0 ){
              waitingForDelay = false;
              audioSource.time = debugTimeSkip;
              videoPlayer.time = debugTimeSkip;
              videoPlayer.Play();
              audioSource.Play();
            }
            #endif

          }
        }

        // First condition: waiting for the video delay to elapse
        // Skip this if debugging
        else if (waitingForDelay)
        {
            if (audioFirst)
              {
                  if (audioSource.time > -audioStartDelay)
                  {
                      videoPlayer.Play();
                      waitingForDelay = false;
                  }
              }
              else // video first, see if playback is done
              {
                  if (videoPlayer.clockTime > audioStartDelay)
                  {
                      audioSource.Play();
                      waitingForDelay = false;
                  }
              }

        }

        // Second condition: playing video, querying with SAGAT
        else if (!videoOver)
        {

            if ((long)videoPlayer.frameCount - videoPlayer.frame < 20)
            {
                // Finished, wrap it up
                if (videoPlayer.isPrepared){
                  Logger.Instance.Log("Video finished");
                  canvasFader.FinishedDisplay();
                  videoOver = true;
                }
            }

            else if (!sagatUnderway){
                // Regular video playback, wait for the time to administer SAGAT
                if (!sagat1Done)
                {
                    // check to administer sagat 1
                    if (videoPlayer.clockTime > sTime1)
                    {
                        StartSagat(1);
                        sagat1Done = true;
                        if (TestParameters.trialSession) sagat2Done = true; // only one sagat this time
                    }
                }
                else if (!sagat2Done)
                {
                    // check to administer sagat 2
                    if (videoPlayer.clockTime > sTime2)
                    {
                        StartSagat(2);
                        sagat2Done = true;
                    }
                }

            }
        }
        else if (videoOver)
        {
            if (canvasFader.currentlyFading == Fader.Fading.Here)
            {
                if (TestParameters.trialSession) WrapItUp("Video complete");
                else
                {
                    videoPlayer.Stop();
                    audioSource.Stop();
                }
            }
            else
            {
                if (canvasFader.currentlyFading != Fader.Fading.In) canvasFader.FadeIn();
            }
        }
    }


    public void StartSagat(int sNumber)
    {
        Logger.Instance.Log("Starting SAGAT "+sNumber);
        videoPlayer.Pause();
        audioSource.Pause();
        canvasFader.FadeIn();
        sagatUnderway = true;
        sagatManager.StartSagat(sNumber);
    }

    public void EndSagat()
    {
        Logger.Instance.Log("SAGAT Complete");
        canvasFader.FadeOut();
        sagatUnderway = false;
        videoPlayer.Play();
        audioSource.Play();
    }

    public void WrapItUp(string endMessage)
    {
        videoPlayer.Stop();
        audioSource.Stop();

        // If it's the trial, move to transition scene. Otherwise write out and close
        if (trialSession)
        {
            SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            // Alright pack it in boys, we're outta here
            Logger.Instance.WriteOut(endMessage);
            if (!Application.isEditor) System.Diagnostics.Process.GetCurrentProcess().Kill();
            Application.Quit();
        }
    }

    [ContextMenu("Start SAGAT (Debug)")]
    public void DebugSAGATStart()
    {
        StartSagat(0);
    }

    protected static string GetArg(string name)
    {
        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == name && args.Length > i + 1)
            {
                return args[i + 1];
            }
        }
        return null;
    }
}
