using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    public enum Fading {In, Out, Here, Gone};

    public bool DebugFader = false;

    public float fadeRate;
    private CanvasGroup cg;
    public Fading currentlyFading = Fading.Gone;


    // Start is called before the first frame update
    void Start()
    {
        cg = gameObject.GetComponent<CanvasGroup>();

    }

    // Update is called once per frame
    void Update()
    {
        // see if we need to change the fade condition
        switch (currentlyFading) {
            case Fading.In:
                // activate canvas if its not already
                SetCanvasInteraction(true);

                // Check if we've reached full opacity
                if (cg.alpha >= 1f)
                {
                    cg.alpha = 1f;
                    currentlyFading = Fading.Here;
                    if (DebugFader){
                        currentlyFading = Fading.Out;
                    }
                }
                else
                {
                    cg.alpha += fadeRate * Time.deltaTime;
                }
                break;
            case Fading.Out:
                // check if we've reached zero opacity
                if (cg.alpha <= 0f)
                {
                    cg.alpha = 0f;
                    currentlyFading = Fading.Gone;
                    SetCanvasInteraction(false);
                }
                else
                {
                    cg.alpha -= fadeRate * Time.deltaTime;
                }
                break;
            default:
                if (cg.alpha != 1f && cg.alpha != 0f)
                {
                    cg.alpha = currentlyFading == Fading.Here ? 1f : 0f;
                }
                break;
        }
    }

    public void FadeIn()
    {
        currentlyFading = Fading.In;
    }

    public void FadeOut()
    {
        currentlyFading = Fading.Out;
    }

    public void FinishedDisplay()
    {
        // First hide gui elements
        CanvasGroup SAGATCanvas = transform.Find("SAGAT").GetComponent<CanvasGroup>();
        SAGATCanvas.alpha = 0f;
        SAGATCanvas.interactable = false;
        SAGATCanvas.blocksRaycasts = false;
        GetComponentInChildren<Text>().text = TestParameters.trialSession ? "" : "Experiment complete";
        FadeIn();
    }

    public void SetCanvasInteraction(bool enabled)
    {
        cg.interactable = enabled;
        cg.blocksRaycasts = enabled;
    }

}
