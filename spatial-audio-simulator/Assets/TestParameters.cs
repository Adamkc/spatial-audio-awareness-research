using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestParameters : MonoBehaviour
{
    public static bool trialSession = true;
    public static string participantId;

    void Awake()
    {
        DontDestroyOnLoad(this);
        trialSession = true;
    }

    public static void SetId(string id){
        participantId = id;
    }

    public static string ToStringPlease(){
        string s = "";
        s += trialSession ? "Trial session" : "Test session";
        s += ", Participant ID: "+participantId;
        return s;

    }


}
