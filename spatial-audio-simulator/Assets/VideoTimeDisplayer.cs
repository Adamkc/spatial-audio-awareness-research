using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoTimeDisplayer : MonoBehaviour
{
    private Text timerText;
    public VideoPlayer videoPlayer;

    // Start is called before the first frame update
    void Start()
    {
        timerText = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        double currentTime = videoPlayer.clockTime;
        int minutes = (int) currentTime / 60;
        int seconds = (int) currentTime % 60;
        timerText.text = minutes.ToString("00") + ":" + seconds.ToString("00");
    }
}
