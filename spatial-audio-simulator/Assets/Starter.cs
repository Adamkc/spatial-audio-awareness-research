using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Starter : MonoBehaviour
{

    public InputField idText;
    public bool startOnClick = true;
    public GameObject explanation;
    public GameObject participantInputField;
    private Text buttonText;

    void Start()
    {
        buttonText = gameObject.GetComponentInChildren<Text>();
        buttonText.text = "Continue";
        explanation.SetActive(false);
        participantInputField.SetActive(true);
    }


    public void ButtonPress()
    {
        if (buttonText.text == "Continue")
        {
            GetIDAndSwitch();
        }
        else
        {
            LetsGo();
        }
    }

    public void LetsGo()
    {
        // First stick in those params
        Debug.Log(TestParameters.ToStringPlease());
        if (idText.text != "" && startOnClick)
        {
            buttonText.text = "Loading...";
            SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public void GetIDAndSwitch()
    {
        participantInputField.SetActive(false);
        explanation.SetActive(true);
        buttonText.text = "Begin trial session";

    }
}
