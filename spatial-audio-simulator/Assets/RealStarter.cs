using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RealStarter : MonoBehaviour
{

    public bool startOnClick = true;

    public void LetsGo()
    {
        // First stick in those params
        TestParameters.trialSession = false;

        Debug.Log(TestParameters.ToStringPlease());
        if (startOnClick) {
            gameObject.GetComponentInChildren<Text>().text = "Loading...";
            SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
