using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButtonIconManager : MonoBehaviour
{

    public Sprite[] actorIcons;
    public int smallAmount = 30;
    public int smallTopValue = 0;

    // Start is called before the first frame update
    void Start()
    {
        smallTopValue = 0;
          //smallTopValue = -t.rect.height + smallAmount;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateIcons()
    {
        foreach (Transform answerToggle in transform)
        {
            foreach (Transform children in answerToggle)
            {
                if (children.gameObject.name == "Icon")
                {
                    // Got the icon here, now do stuff
                    Text nameText = answerToggle.GetComponentInChildren<Text>();
                    string name = nameText.text;
                    Sprite icon = GetIcon(name);
                    if (icon != null)
                    {
                        // ok we gottem. change icon and lower text position
                        children.gameObject.GetComponentInChildren<Image>().sprite = icon;
                        children.gameObject.GetComponentInChildren<Image>().enabled = true;
                        SetTopFromBottom(nameText.transform.GetComponent<RectTransform>());

                    }
                    else
                    {
                        children.gameObject.GetComponentInChildren<Image>().enabled = false;
                        ResetTop(nameText.transform.GetComponent<RectTransform>());
                    }
                }
            }
        }

    }

    public List<string> IconNames()
    {
        List<string> names = new List<string>();

        foreach (Sprite s in actorIcons){
            names.Add(s.name);
        }

        return names;
    }

    public Sprite GetIcon(string name)
    {
        foreach (Sprite s in actorIcons){
            if (name == s.name) return s;
        }
        return null;
    }


    public void SetTopFromBottom(RectTransform t)
    {
        Debug.Log("Rect height: "+t.rect.height+", smallTopValue = "+smallTopValue );

        if (smallTopValue == 0)
        {
            Debug.Log("Resetting smallTopValue (was "+smallTopValue+")");
            smallTopValue = (int)(-t.rect.height + smallAmount);
        }
        SetTop(t, smallTopValue);
    }

    public void SetTop(RectTransform t, float top){
        t.offsetMax = new Vector2(t.offsetMax.x, top);
    }

    public void ResetTop(RectTransform t){
        t.offsetMax = new Vector2(t.offsetMax.x, 0);
    }

    [ContextMenu("Make Top Small")]
    public void DebugSetTopSmall(){
        foreach (Transform answerToggle in transform)
        {
            foreach (Transform children in answerToggle)
            {
                if (children.gameObject.name == "Icon")
                {
                    // Got the icon here, now do stuff
                    Text nameText = answerToggle.GetComponentInChildren<Text>();
                    SetTopFromBottom(nameText.transform.GetComponent<RectTransform>());

                }
            }
        }

    }

    [ContextMenu("Make Top Large")]
    public void DebugSetTopLarge(){
        foreach (Transform answerToggle in transform)
        {
            foreach (Transform children in answerToggle)
            {
                if (children.gameObject.name == "Icon")
                {
                    // Got the icon here, now do stuff
                    Text nameText = answerToggle.GetComponentInChildren<Text>();
                    ResetTop(nameText.transform.GetComponent<RectTransform>());

                }
            }
        }
    }

}
