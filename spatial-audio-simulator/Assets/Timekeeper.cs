using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timekeeper : MonoBehaviour
{
    private static Timekeeper instance;

    public static Timekeeper Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(Timekeeper)) as Timekeeper;

            return instance;
        }

    }

    [System.Serializable]
    public struct ClipQuestions
    {
        public float[] time;
        public string[] question;
        public string[] target;
        public string[] distractor;
        public string[] targetAnswer;
        public string[] distractorAnswer;
        public bool[] targetCorrect;

    }

    public struct ClipQuestion
    {
        public float time;
        public string question;
        public string target;
        public string distractor;
        public string targetAnswer;
        public string distractorAnswer;
        public bool targetCorrect;
    }

    public bool logQuestionWhileHappening = false;

    [ShowOnlyAttribute]
    public bool clip1Selected;

    public ClipQuestions clip1;
    public float trialDifference1;

    public ClipQuestions clip2;
    public float trialDifference2;

    [HideInInspector]
    public ClipQuestions currentClip;

    private int index = 0;

    //[HideInInspector]
    public Dictionary<string,int> scores;

    private double lastTime;
    private double time;


    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        clip1Selected = ExperimentManager.Instance.clip1Selected;
        currentClip = clip1Selected ? clip1 : clip2;

        Debug.Log("Timekeeper: Clip "+ (clip1Selected ? "1" : "2") +" selected.");

        scores = new Dictionary<string, int>();
        scores.Add("Carlos", 0);
        scores.Add("John", 0);
        scores.Add("Elizabeth", 0);
        scores.Add("Annie", 0);
        scores.Add("Susan", 0);
        scores.Add("Maria", 0);
        scores.Add("Megan", 0);
        scores.Add("Michael", 0);
        scores.Add("Emma", 0);
        scores.Add("Amy", 0);
        scores.Add("Carol", 0);
        scores.Add("Paul", 0);

        lastTime = Time.time;

        if (TestParameters.trialSession)
        {
            float diff = clip1Selected ? trialDifference1 : trialDifference2;

            Debug.Log("Trial session detected, modifying timestamps accordingly");
            for (int i=0; i<currentClip.time.Length; i++)
            {
                currentClip.time[i] -= diff;
            }
        }

    }

    void Reset()
    {
        if (index != 0)
        {
          lastTime = Time.time;
          index = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {

        string sceneName = SceneManager.GetActiveScene().name;

        if (sceneName != "PerformanceScene" && sceneName != "TrialScene")
        {
            // transition scene, reset
            Reset();
        }
        else
        {
            // get timestamp from video
            time = ExperimentManager.Instance.audioSource.time;

            if (time > Mathf.Round((float)lastTime+5)){
                Logger.Instance.Log("Video time: "+(Mathf.Round((float)time * 10.0f) * 0.1f));
                lastTime = time;
            }


            if (index < currentClip.time.Length && time > currentClip.time[index])
            {
                // Log the update

                if (currentClip.targetCorrect[index]) scores[currentClip.target[index]]++;
                else  scores[currentClip.distractor[index]]++;

                if (logQuestionWhileHappening)
                {
                    int i = index;
                    string logstring = "Quiz question: <"+currentClip.question[i]+">, ";
                    logstring += "Target: <"+currentClip.target[i]+">, ";
                    logstring += "Distractor: <"+currentClip.distractor[i]+">, ";
                    logstring += "Target_Answer: <"+currentClip.targetAnswer[i]+">, ";
                    logstring += "Distractor_Answer: <"+currentClip.distractorAnswer[i]+">, ";
                    logstring += "Target_correct: <"+currentClip.targetCorrect[i]+">";
                    Logger.Instance.Log(logstring);
                }

                index++;
            }
        }
    }

    public void UpdateClip()
    {
        currentClip = clip1Selected ? clip1 : clip2;
    }


    public int GetIndex(){
      return index;
    }

    public double GetTime(){
      return time;
    }

    public ClipQuestion GetFullQuestion(int i){
        ClipQuestion c;
        c.time = currentClip.time[i];
        c.question = currentClip.question[i];
        c.target = currentClip.target[i];
        c.distractor = currentClip.distractor[i];
        c.targetAnswer = currentClip.targetAnswer[i];
        c.distractorAnswer = currentClip.distractorAnswer[i];
        c.targetCorrect = currentClip.targetCorrect[i];
        return c;
    }

    public ClipQuestion GetLastQuestion(){
        return GetFullQuestion(index-1);
    }

    public ClipQuestion[] GetAllPriorQuestions(){
        //Debug.Log("GetAllPriorQuestions");
        ClipQuestion[] cqs = new ClipQuestion[index];

        for (int i = index-1; i >= 0; i--){
            cqs[i] = GetFullQuestion(i);
        }

        return cqs;

    }

    public string GetQuestion(int i){
        return currentClip.question[i];
    }

    public string GetTargetAnswer(int i){
        return currentClip.targetAnswer[i];
    }

    public string GetDistractorAnswer(int i){
        return currentClip.distractorAnswer[i];
    }

    public string GetTargetName(int i){
        return currentClip.target[i];
    }

    public string GetDistractorName(int i){
        return currentClip.distractor[i];
    }

    public bool TargetCorrect(int i){
        return currentClip.targetCorrect[i];
    }

    public string GetRightAnswer(int i){
        return currentClip.targetCorrect[i] ? currentClip.targetAnswer[i] : currentClip.distractorAnswer[i];
    }
}
