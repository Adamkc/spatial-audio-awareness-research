using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Retake : MonoBehaviour
{

    public void RetakeTrial()
    {
        Logger.Instance.LogBypass("Trial retaken");
        SceneManager.LoadScene("TrialScene");
    }
}
