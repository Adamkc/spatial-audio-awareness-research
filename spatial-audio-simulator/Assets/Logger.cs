using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logger : MonoBehaviour
{
    private static Logger _instance;

    public static Logger Instance
    {
        get { return _instance; }
    }

    private string logged_data = "";
    public string outfile = "";
    public bool log = true;

    [HideInInspector]
    public bool wroteOut = false;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void Update()
    {
        // Log key presses
        foreach (string keyname in new List<string> {"left", "right"})
        {
            if (Input.GetKeyDown(keyname)){
              Log(keyname+" pressed");
            }
            else if (Input.GetKeyUp(keyname)){
              Log(keyname+" released");
            }
        }
    }

    public void Log(string message)
    {
        message = "["+ System.DateTime.Now + ":"+System.DateTime.Now.Millisecond + "] " + message;
        if (!TestParameters.trialSession)
        {

            //message = "["+ System.DateTime.Now + ":"+System.DateTime.Now.Millisecond + "] " + message;
            Debug.Log("Logged: "+message);
            this.logged_data += message+"\n";

        }
        else Debug.Log("Not logged: "+message);
    }

    public void LogBypass(string message)
    {
        message = "["+ System.DateTime.Now + ":"+System.DateTime.Now.Millisecond + "] " + message;

        //message = "["+ System.DateTime.Now + ":"+System.DateTime.Now.Millisecond + "] " + message;
        Debug.Log("Logged (bypass): "+message);
        this.logged_data += message+"\n";


    }

    public void WriteOut(string message)
    {
        LogBypass(message);
        WriteOut();
    }

    public void WriteOut()
    {

        if (!wroteOut)
        {
          // Find filename
          LogBypass("Log creation for "+(TestParameters.trialSession ? "trial session..." : "experiment proper..."));

          Debug.Log("Writing out");
          string file = Application.dataPath;
          string[] pathArray = file.Split('/');
          file="";
          for(int i=0 ; i<pathArray.Length-1 ; i++){
              file += pathArray[i]+"/";
          }

          string ID = "NULL";
          if (TestParameters.participantId != null) ID = int.Parse(TestParameters.participantId).ToString("000");

          file += ("LOG_"+ID+"_"+System.DateTime.Now+".txt").Replace("/","-").Replace(":","-");

          Debug.Log("Writing out "+logged_data.Length+" characters to "+file);

          System.IO.File.WriteAllText(file, logged_data);
          if (!TestParameters.trialSession)  wroteOut = true;
        }
    }

    [ContextMenu("Test Logger")]
    public void TestLog()
    {
        Log("Logger test");
    }
}
