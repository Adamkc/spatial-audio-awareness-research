# Spatial Audio Awareness Research

Unity simulator allowing immersive spatial audio and 360deg video, as well as R scripts to process recorded data, for a publication on spatial audio and social situation awareness.
