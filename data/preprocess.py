#!/usr/bin/env python3

from os import listdir
from math import sqrt

def timestamp_sec(timestamp):
    if "[" in timestamp:
        timestamp = timestamp.split("]")[0].split(" ")[1]
    [h,m,s,ms] = timestamp.split(":")
    return (int(ms)/1000.0 + int(s) + 60.0*int(m) + 60.0*60.0*int(h))


condition_count = {}
condition_count["1Spatial"] = 0
condition_count["1Mono"] = 0
condition_count["2Spatial"] = 0
condition_count["2Mono"] = 0
scene_count = {}
scene_count["1"] = 0
scene_count["2"] = 0
audio_count = {}
audio_count["Mono"]=0
audio_count["Spatial"]=0
data_source = {}
data_source["data_digitalhub"] = 0
data_source["data_tcd"] = 0

filecount = 0

header_values = ["id", "scene", "audio", "sagat_score", "performance_score", "average_time_to_answer", "time_in_motion", "angle_mean", "angle_variance"]

with open("compiled_data.csv", "w") as f:
    f.write(",".join(header_values)+"\n")

for current_dir in ["data_tcd", "data_digitalhub"]:
    files = [x for x in listdir(current_dir) if "LOG" in x and "FAILED" not in x]
    files.sort()

    #print(current_dir)
    for file in files:
        #print(file)
        id = ""
        scene = ""
        audio = ""
        sagat_1 = ""
        sagat_2 = ""
        with open(current_dir+"/"+file, "r") as f:
            data = f.read().split("\n")
        id = data[0].split("Participant ID: ")[1]
        scene = data[2].split("Scene: Scene ")[1]
        audio = data[3].split("Audio: ")[1]
        sagat_line = data[5].split("Got SAGAT times: ")[1]
        sagat_1 = sagat_line.split(" and ")[0].replace("s","")
        sagat_2 = sagat_line.split(" and ")[1].replace("s","")

        # Got basic data. Let's print it
        audio_count[audio] += 1
        scene_count[scene] += 1
        condition_count[scene+audio] += 1
        data_source[current_dir] += 1
        filecount += 1

        # now lets extract more complex stuff.

        # First, time in motion
        in_motion = False

        start_time = timestamp_sec(data[0])
        end_time = timestamp_sec([x for x in data if "Video finished" in x][0])
        total_duration = end_time-start_time

        seconds_array_moving = []
        seconds_array_angle = []
        motion_time = 0.0
        prev_time = start_time
        current_angle = 0.0
        sagat_score = 0
        perf_score = 0
        perf_questions = 0
        q_time_total = 0
        for line in data:
            if ":" in line:
                time = timestamp_sec(line)

            if "SAGAT answer given:" in line and "(correct" in line:
                sagat_score += 1

            if "RightAnswerTask: Showing" in line:
                question_start_time = time
            elif "RightAnswerTask:" in line and "Ready" not in line:
                q_time_total += time-question_start_time

            if "RightAnswerTask: Correct" in line:
                perf_score +=1


            if "Camera angle: " in line:
                current_angle = float(line.split("Camera angle: ")[1])

            if "right pressed" in line or "left pressed" in line:
                in_motion = True
                started_moving = time
            elif "right released" in line or "left released" in line or "Starting SAGAT" in line:
                if in_motion:
                    in_motion = False
                    motion_time += (time - started_moving)

            if time >= prev_time + 1.0:
                prev_time += 1.0
                seconds_array_angle += [((int(current_angle)+180)%360) - 180]
                seconds_array_moving += [1 if in_motion else 0]

        index = range(len(seconds_array_moving))
        mvt_profile_out = "time,moving,angle;\n"

        for i in index:
            mvt_profile_out += str(i)+","+str(seconds_array_moving[i])+","+str(seconds_array_angle[i])+";\n"

        mpfile = "mprofile_"+id.zfill(3)+".csv"
        with open("motion_profiles/"+mpfile, "w") as f:
            f.write(mvt_profile_out)

        # Variance of angle
        angle_mean = sum(seconds_array_angle)/len(seconds_array_angle)
        angle_variance = sum([(angle-angle_mean)*(angle-angle_mean) for angle in seconds_array_angle])/len(seconds_array_angle)

        ratio_of_time_in_motion = motion_time / total_duration
        #print(id,")",scene,audio.zfill(7)," - ",str(int(total_duration)),"s, Time in motion:",int(ratio_of_time_in_motion*100),"%")

        # append to file
        # ["id", "scene", "audio", "sagat_score", "performance_score", "average_time_to_answer", "time_in_motion", "angle_mean", "angle_variance"]
        compiled = [id, scene, audio, sagat_score, perf_score, q_time_total/10.0, ratio_of_time_in_motion, angle_mean, sqrt(angle_variance)]
        with open("compiled_data.csv", "a") as f:
            f.write(",".join([str(x) for x in compiled])+"\n")


print("Total files processed:",filecount)
print("Data source:",data_source)
print("Audio condition:",audio_count)
print("Scene:",scene_count)
print("Test conditions:",condition_count)
